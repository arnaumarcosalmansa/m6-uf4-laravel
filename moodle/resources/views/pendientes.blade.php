@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Examenes pendientes</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (Auth::user()->rol == 'alum')
                    <div>
                        @foreach ($pendientes as $pendiente)
                            <div>
                                <a href="{{ url("/do/{$pendiente->id}") }}"><strong>{{ $pendiente->titulo }}</strong></a>
                            </div>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
