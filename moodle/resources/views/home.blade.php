@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <!-- You are logged in! -->
                    @if (Auth::user()->rol == 'prof')
                        <p>
                            <a role="button" href="{{ url("/create") }}" class="btn btn-primary">Crear examen</a>
                        </p>
                        <div>
                        <h2>Mis examenes</h2>
                        @foreach ($exams as $exam)
                            <div>
                                <a href="{{ url("/edit/{$exam->id}") }}"><strong>{{ $exam->titulo }}</strong> (click para editar) </a>
                                <span style="float: right;">Nota : {{ $exam->nota }}</span>
                                <table class="table">
                                <thead><tr><th>Número</th><th>Enunciado</th><th>Respuesta</th><th>Puntos</th></tr></thead>
                                <tbody>
                                @foreach ($exam->preguntas as $pregunta)
                                    <tr>
                                        <td class="form-group">{{ $pregunta->numero_pregunta }}</td>
                                        <td class="form-group">{{ $pregunta->enunciado }}</td>
                                        <td class="form-group">{{ $pregunta->respuesta }}</td>
                                        <td class="form-group">{{ $pregunta->puntos }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <table>
                            </div>
                        @endforeach
                        </div>
                    @endif
                    @if (Auth::user()->rol == 'alum')
                    <p>
                        <a href="{{ url("/pendientes") }}"><strong>Examenes pendientes</strong></a>
                    </p>
                    <div>
                        @foreach ($exams as $exam)
                            <div>
                                <strong>{{ $exam->modelo->titulo }}</strong>
                                <span style="float: right;">{{ $exam->nota }} / {{ $exam->modelo->nota }}</span>
                                <table class="table">
                                <thead><tr><th>Número</th><th>Enunciado</th><th>Respuesta Correcta</th><th>Respuesta</th><th>Puntos</th><th>Correcta</th></tr></thead>
                                <tbody>
                                @foreach ($exam->preguntas as $pregunta)
                                    <tr>
                                        <td class="form-group">{{ $pregunta->modelo->numero_pregunta }}</td>
                                        <td class="form-group">{{ $pregunta->modelo->enunciado }}</td>
                                        <td class="form-group">{{ $pregunta->modelo->respuesta }}</td>
                                        <td class="form-group">{{ $pregunta->respuesta }}</td>
                                        <td class="form-group">{{ ($pregunta->correcta > 0 ? $pregunta->modelo->puntos : '0') }} / {{ $pregunta->modelo->puntos }}</td>
                                        <td class="form-group">{{ ($pregunta->correcta > 0 ? 'Sí' : 'No') }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <table>
                            </div>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
