<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExamenModelo;
use App\PreguntaModelo;
use App\ExamenIntento;
use App\PreguntaIntento;
use App\Exceptions\LimiteDeIntentosExcedido;
use App\Exceptions\NoEsElPropietarioException;
use Auth;
use DB;

class ExamenController extends Controller
{
    //
    //mostrar vista creacion de un examen
    public function create()
    {
        if($view = parent::kickout())
        {
            return $view;
        }
        //tienes que ser profesor
        if(Auth::user()->rol == 'prof')
        {
            return view('crearexamen');
        }
        else
        {
            return redirect('/');
        }
    }

    //guardamos el examen tanto si lo acabamos de crear como si lo hemos editado
    public function save(Request $request)
    {
        if($view = parent::kickout())
        {
            return $view;
        }
        //tienes que ser profesor
        if(Auth::user()->rol == 'prof')
        {
            try
            {
                //parseamos y reevaluamos todos los intentos
                $examen = $this->parseExamen($request);

                $this->reevaluarTodos($examen);
            }
            catch(NoEsElPropietarioException $e)
            {
                //vuelve al home
            }
        }

        return redirect('/');
    }

    //recuperar el examen del formulario
    private function parseExamen(Request $request)
    {
        $examen = new ExamenModelo();
        //si el formulario devuelve un id recuperamos el examen de la base de datos
        if($request->input('examen_id'))
        {
            $examen = ExamenModelo::find($request->input('examen_id'));
            //comprobamos que seas el autor del examen
            if($examen->user_id != Auth::user()->id)
            {
                throw new NoEsElPropietarioException();
            }
        }
        $examen->id = $request->input('examen_id') ?? null;
        //llenamos el examen de datos
        $examen->user_id = Auth::user()->id;
        $examen->titulo = $request->input('titulo');
        $examen->nota = 0;
        //guardamos para que se genere el id o actualizamos
        $examen->save();

        //recuperamos cada pregunta
        $i = 0;
        foreach($request->input('enunciado') as $enunciado)
        {
            $pregunta = new PreguntaModelo();

            if($request->input('pregunta_id.' . $i))
            {
                //la recuperamos de base de datos si existe
                $pregunta = PreguntaModelo::find($request->input('pregunta_id.' . $i));
            }
            //metemos los datos en la pregunta modelo
            $pregunta->id = $request->input('pregunta_id.' . $i) ?? null;
            $pregunta->enunciado = $enunciado;
            $pregunta->numero_pregunta = ($i + 1);
            $pregunta->puntos = $request->input('puntos.' . $i);
            $pregunta->respuesta = $request->input('respuesta.' . $i);
            //guardamos la pregunta relacionandola al examen
            $examen->preguntas()->save($pregunta);
            //sumamos la nota
            $examen->nota += $pregunta->puntos;
            $i++;
        }
        //guardamos para que se inserte la nota
        $examen->save();
        //recargamos para que cargue las preguntas
        $examen->refresh();
        return $examen;
    }

    //para editar el examen
    public function edit(ExamenModelo $examen)
    {
        if($view = parent::kickout())
        {
            return $view;
        }
        //tienes que ser profesor y ser el autor del examen
        if(Auth::user()->rol == 'prof' && $examen->user_id == Auth::user()->id)
        {
            return view('editarexamen', compact('examen'));
        }
        else
        {
            return view('welcome');
        }
    }

    //mostrar el ranking
    public function rank()
    {
        if($view = parent::kickout())
        {
            return $view;
        }
        //ambas consultas funcionan
        //consulta para recoger los mejores examenes ordenados por examen modelo
        $exams = ExamenIntento::whereRaw('(user_id, examen_modelo_id, nota) IN (SELECT user_id, examen_modelo_id, max(nota) FROM examen_intentos GROUP BY user_id, examen_modelo_id) ORDER BY examen_modelo_id, nota DESC')->get();
        
        //consulta para recoger los mejores examenes ordenados por examen modelo
        $exams = ExamenIntento::whereIn(DB::raw('(user_id, examen_modelo_id, nota)'), function($query) {
            $query->selectRaw('user_id, examen_modelo_id, max(nota)')
                ->from('examen_intentos')
                ->groupBy('user_id', 'examen_modelo_id')
                ->get();
        })
        ->orderBy('examen_modelo_id', 'ASC')
        ->orderBy('nota', 'DESC')
        ->get();

        //por si hay multiples intentos con la nota más alta
        //por si un alumno tiene 2 examen con la misma nota que es la más alta
        $exams = $exams->unique(function ($exam) {
            return $exam->user_id . $exam->examen_modelo_id . $exam->nota;
        });

        return view('ranking', compact('exams'));
    }

    //obtener un examen para intentarlo
    public function intentar(ExamenModelo $examen)
    {
        if($view = parent::kickout())
        {
            return $view;
        }
        //tienes que ser alumno
        if(Auth::user()->rol == 'alum')
        {
            //check del limite de intentos
            try
            {
                $this->checkLimiteIntentos($examen);

                return view('intentar', compact('examen'));
            }
            catch(LimiteDeIntentosExcedido $e)
            {
                //si excede el limite de intentos, vuelve al home
            }
        }

        return redirect('/');
    }

    //entregar el examen
    public function entregar(ExamenModelo $examen, Request $request)
    {
        if($view = parent::kickout())
        {
            return $view;
        }

        try
        {
            //parseamos el intento y lo evaluamos
            $intento = $this->parseIntento($examen, $request);

            $intento = $this->evaluar($examen, $intento);
            //guardamos el intento ya corregido
            $examen->intentos()->save($intento);
            //guardamos las preguntas
            foreach ($intento->preguntas as $pregunta)
            {
                $intento->preguntas()->save($pregunta);
            }
        }
        catch(LimiteDeIntentosExcedido $e)
        {
            //vuelve al home
        }
        return redirect('/');
    }

    private function parseIntento(ExamenModelo $examen, Request $request)
    {
        $numeroIntentosPrevios = $this->checkLimiteIntentos($examen);

        $intento = new ExamenIntento();
        $intento->user_id = Auth::user()->id;
        $intento->numero_intento = $numeroIntentosPrevios + 1;

        $i = 0;
        //recuperamos las preguntas del formulario
        foreach ($request->input('id_pregunta') as $idPregunta) {
            //si la pregunta que hemos respondido es del examen
            if ($examen->preguntas->contains($idPregunta)) {
                //creamos una pregunta de intento nueva y la llenamos
                $pregunta = new PreguntaIntento();
                $pregunta->respuesta = $request->input('respuesta.' . $i);
                $pregunta->correcta = false;
                $pregunta->pregunta_modelo_id = $idPregunta;
                $intento->preguntas->push($pregunta);
            }
            $i++;
        }

        return $intento;
    }

    //miramos que el examen se haya intentado menos de 3 veces
    private function checkLimiteIntentos(ExamenModelo $examen)
    {
        $numeroIntentosPrevios = ExamenIntento::where('user_id', '=', Auth::user()->id)->where('examen_modelo_id', '=', $examen->id)->count();

        if ($numeroIntentosPrevios > 2) {
            throw new LimiteDeIntentosExcedido();
        }
        return $numeroIntentosPrevios;
    }

    //reevalua todos los intentos de un examen modelo
    private function reevaluarTodos(ExamenModelo $modelo)
    {
        $intentos = $modelo->intentos;

        //reevaluamos cada intento
        foreach ($intentos as $intento) {
            $evaluado = $this->evaluar($modelo, $intento);
            //guardamos el intento
            $evaluado->save();

            //guardamos cada pregunta
            foreach ($evaluado->preguntas as $pregunta) {
                $evaluado->preguntas()->save($pregunta);
            }
        }
    }

    //funcion para evaluar un examen
    private function evaluar(ExamenModelo $modelo, ExamenIntento $intento)
    {
        $modelos = $modelo->preguntas;
        $intentos = $intento->preguntas;

        $notaTotal = 0;
        $len = count($modelo->preguntas);
        //recorremos todas las preguntas
        for ($i = 0; $i < $len; $i++) {
            $respuestamodelo = $modelos[$i]->respuesta;
            $respuestaintento = $intentos[$i]->respuesta;

            //marcamos como correcta o incorrecta
            if ($respuestamodelo == $respuestaintento) {
                $intentos[$i]->correcta = true;
                $notaTotal += $modelos[$i]->puntos;
            } else {
                $intentos[$i]->correcta = false;
            }
        }

        $intento->nota = $notaTotal;
        return $intento;
    }

    //un poco de extra
    //listar todos los examenes que no has intentado
    public function pendientes()
    {
        if($view = parent::kickout())
        {
            return $view;
        }

        if(Auth::user()->rol == 'alum')
        {
            //examenes que no has intentado
            $pendientes = ExamenModelo::whereNotIn('id', DB::table('examen_intentos')->select('examen_modelo_id')->where('user_id', '=', Auth::user()->id))->get();
            return view('pendientes', compact('pendientes'));
        }
        else
        {
            return redirect('/');
        }
    }
}
