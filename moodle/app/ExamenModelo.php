<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamenModelo extends Model
{
    //
    public function intentos()
    {
        return $this->hasMany(ExamenIntento::class);
    }

    public function preguntas()
    {
        return $this->hasMany(PreguntaModelo::class);
    }

    public function creador()
    {
        return $this->belongsTo(User::class);
    }
}
