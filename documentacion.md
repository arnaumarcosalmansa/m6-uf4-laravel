# Documentación

1. Las rutas son las especificadas en el enunciado más la ruta /edit/{id} para editar un examen.
2. Para acceder a algunas partes hay que escribir la URL manualmente.
3. El código está comentado.
4. La ruta /save con la función ExamenController@save se llama tanto al crear un examen nuevo como al editarlo y guardarlo.
5. Credenciales de la base de datos:     usuario -> admin     contraseña -> super3     database -> laravel