@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('save_exam') }}">
                        @csrf
                        <input type="hidden" name="examen_id" value="{{ $examen->id }}">
                        <div><label>Titulo: </label><input type="text" name="titulo" value="{{ $examen->titulo }}"></div>
                        <table class="table">
                            <thead><tr><th>Número</th><th>Enunciado</th><th>Respuesta</th><th>Puntos</th></tr></thead>
                            <tbody>
                            @foreach ($examen->preguntas as $pregunta)
                                <tr>
                                    <input type="hidden" name="pregunta_id[]" value="{{ $pregunta->id }}">
                                    <td class="form-group"><input class="form-field" type="text" name="numero[]" value="{{ $pregunta->numero_pregunta }}" disabled></td>
                                    <td class="form-group"><input class="form-field" type="text" name="enunciado[]" value="{{ $pregunta->enunciado }}"></td>
                                    <td class="form-group"><input class="form-field" type="text" name="respuesta[]" value="{{ $pregunta->respuesta }}"></td>
                                    <td class="form-group"><input class="form-field" type="number" name="puntos[]" value="{{ $pregunta->puntos }}"></td>
                                    <!-- <td><span onclick="deletef(this.parentNode.parentNode)">Eliminar</span></td> -->
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <script>
                            function deletef(element)
                            {
                                const parent = element.parentNode;
                                parent.removeChild(element);
                            }
                        </script>
                        <input type="submit" value="Guardar">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
