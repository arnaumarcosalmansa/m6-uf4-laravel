@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Intentar examen {{ $examen->titulo }}</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ url("/do/{$examen->id}") }}">
                        @csrf
                        @foreach ($examen->preguntas as $pregunta)
                            <article>
                                <h4>{{ $pregunta->numero_pregunta . ' ' . $pregunta->enunciado }}</h4>
                                <input type="hidden" name="id_pregunta[]" value="{{ $pregunta->id }}">
                                <input type="text" name="respuesta[]">
                            </article>
                        @endforeach
                        <input class="btn btn-primary" type="submit" value="Entregar">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
