<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Examen extends Model
{
    //
    public function answers()
    {
        return $this->hasMany('App\Respuesta');
    }

    public function author()
    {
        return $this->belongsTo('App\User');
    }
}
