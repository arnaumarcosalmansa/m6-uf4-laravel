<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreguntaIntentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pregunta_intentos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('examen_intento_id')->unsigned();
            $table->integer('pregunta_modelo_id')->unsigned();
            $table->string('respuesta');
            $table->boolean('correcta');
        });

        Schema::table('pregunta_intentos', function (Blueprint $table) {
            $table->foreign('examen_intento_id')->references('id')->on('examen_intentos');
            $table->foreign('pregunta_modelo_id')->references('id')->on('pregunta_modelos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pregunta_intentos');
    }
}
