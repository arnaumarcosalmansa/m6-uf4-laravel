@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Ranking</div>
                <div class="card-body">
                        @php
                            $last_id = -1;
                        @endphp
                    @foreach ($exams as $exam)
                        @if ($exam->examen_modelo_id != $last_id)
                            @if ($last_id != -1)
                                </ol>
                            @endif
                            <h2>{{ $exam->modelo->id }} -> {{ $exam->modelo->titulo }}</h2>
                            <ol>
                            @php
                                $last_id = $exam->examen_modelo_id;
                            @endphp
                        @endif
                        <li>{{ $exam->alumno->name }} -> {{ $exam->nota }} / {{ $exam->modelo->nota }}</li>
                    @endforeach
                    </ol>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
