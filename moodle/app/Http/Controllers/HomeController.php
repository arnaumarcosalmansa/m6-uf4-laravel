<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\ExamenIntento;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function home()
    {
        if(Auth::check())
        {
            if(Auth::user()->rol == 'prof')
            {
                $exams = Auth::user()->creados;
                return view('home', compact('exams'));
            }
            else if(Auth::user()->rol == 'alum')
            {
                $exams = Auth::user()->intentos;
                $exams = ExamenIntento::with('preguntas.modelo')->where('user_id', '=', Auth::user()->id)->get();
                return view('home', compact('exams'));
            }
            return view('login');

        }
        else
        {
            return view('login');
        }
    }
}
