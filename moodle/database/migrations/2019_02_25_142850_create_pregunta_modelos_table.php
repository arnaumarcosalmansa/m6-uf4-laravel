<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreguntaModelosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pregunta_modelos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('examen_modelo_id')->unsigned();
            $table->integer('numero_pregunta');
            $table->string('enunciado');
            $table->string('respuesta');
            $table->float('puntos', 2, 1);
        });

        Schema::table('pregunta_modelos', function (Blueprint $table) {
            $table->foreign('examen_modelo_id')->references('id')->on('examen_modelos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pregunta_modelos');
    }
}
