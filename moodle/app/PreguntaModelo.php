<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreguntaModelo extends Model
{
    //
    public function intentos()
    {
        return $this->hasMany(PreguntaIntento::class);
    }

    public function examen()
    {
        return $this->belongsTo(ExamenModelo::class, 'examen_modelo_id');
    }
}
