<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreguntaIntento extends Model
{
    //
    public function examen()
    {
        return $this->belongsTo(ExamenIntento::class, 'examen_intento_id');
    }

    public function modelo()
    {
        return $this->belongsTo(PreguntaModelo::class, 'pregunta_modelo_id');
    }
}
