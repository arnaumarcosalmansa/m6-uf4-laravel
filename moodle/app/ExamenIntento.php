<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamenIntento extends Model
{
    //
    public function modelo()
    {
        return $this->belongsTo(ExamenModelo::class, 'examen_modelo_id');
    }

    public function alumno()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function preguntas()
    {
        return $this->hasMany(PreguntaIntento::class);
    }
}
