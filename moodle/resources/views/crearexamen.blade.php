@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('save_exam') }}">
                        @csrf
                        <div><label>Titulo: </label><input type="text" name="titulo"></div>
                        <table id="preguntas" class="table">
                            <thead><tr><th>Número</th><th>Enunciado</th><th>Respuesta</th><th>Puntos</th></tr></thead>
                            <tbody>
                                <tr>
                                    <td class="form-group">1</td>
                                    <td class="form-group"><input class="form-field" type="text" name="enunciado[]"></td>
                                    <td class="form-group"><input class="form-field" type="text" name="respuesta[]"></td>
                                    <td class="form-group"><input class="form-field" type="number" name="puntos[]"></td>
                                </tr>
                            </tbody>
                        <table>
                        <script>
                            let numeroPregunta = 1;

                            function addPregunta()
                            {
                                numeroPregunta++;

                                let preguntaHTML = '<tr>\n<td class="form-group">' + numeroPregunta + '</td>\n<td class="form-group"><input class="form-field" type="text" name="enunciado[]"></td>\n<td class="form-group"><input class="form-field" type="text" name="respuesta[]"></td>\n<td class="form-group"><input class="form-field" type="number" name="puntos[]"></td>\n</tr>';

                                let preguntas = document.getElementById('preguntas').tBodies[0];
                                preguntas.innerHTML += preguntaHTML;
                            }
                        </script>
                        <input class="btn btn-primary" type="submit" value="Guardar">
                        <div class="btn btn-secondary float-right" onclick="addPregunta()" >Añadir pregunta</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
