<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('/', 'HomeController@home');
Auth::routes();

Route::get('/home', 'HomeController@home')->name('home');

Route::get('/create', 'ExamenController@create')->name('create_exam');

Route::get('/edit/{examen}', 'ExamenController@edit')->name('edit_exam');

Route::post('/save', 'ExamenController@save')->name('save_exam');

Route::get('/rank', 'ExamenController@rank');

Route::get('/do/{examen}', 'ExamenController@intentar');
Route::post('/do/{examen}', 'ExamenController@entregar');

Route::get('/pendientes', 'ExamenController@pendientes');
/*
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
*/
